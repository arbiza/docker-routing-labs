# Lab 3

* __Starting:__ _# bash run up_
* __Stopping:__ _# bash run down_
* __Reseting:__ _# bash run reset_

![Lab diagram](conf/images/diagram.png)

After running __bash run up__ as root connect to the routers throught the following:

```bash
$ telnet ip_address port
```
## Information to connect to routers

Addresses:

* Router AS31 R1: 172.10.3.1
* Router AS31 R2: 172.10.3.2
* Router AS31 R3: 172.10.3.3
* Router AS32 R1: 172.10.3.32
* Router AS33 R1: 172.10.3.33

Ports:

* 2601: Zebra daemon - Addressing and static routes configuration
* 2604: OSPFv4 daemon
* 2605: BGP daemon
* 2606: OSPFv6 daemon

## ASN and addressing details

* ASN 31: 2a00:31::/32 and 95.31.0.0/22:
    * Loopbacks:
        * 2a00:31::/64, 95.31.0.0/26
    * Internal networks:
        * 2a00:31:0:f00::/56, 95.31.1.0/24
        * 2a00:31:1::/48, 95.31.2.0/23
* ISP 1: 2804:18::/32 and 95.32.0.0/32:
    * connections and loopbacks:
        * 2a00:32::/48, 95.32.0.0/24
    * internal networks:
        * 2a00:32:a::/64, 95.32.100.0/24
* ASN 33: 2a00:33::/32 and 95.33.0.0/22:
    * connections and loopbacks:
        * 2a00:33::/48, 95.33.0.0/24
    * internal networks:
        * 2a00:33:a::/64, 95.33.100.0/24


### Addressing

__as31_r1:__

* __lo__: 2a00:31::251/128, 95.31.0.251/32
* __eth10__: 2a00:31::1/126, 95.31.0.1/30
* __eth20__: 2a00:31::5/126, 95.31.0.5/30
* __eth30__: 2a00:32::2/126, 95.32.0.2/30
* __eth40__: 2a00:33::2/126, 95.33.0.2/30

__as31_r2:__

* __lo__: 2a00:31::252/128, 95.31.0.252/32
* __eth10__: 2a00:31::2/126, 95.31.0.2/30
* __eth20__: 2a00:31::9/126, 95.31.0.9/30
* __eth30__: 2a00:31:a::1/64, 95.31.100.1/24

__as31_r3:__

* __lo__: 2a00:31::253/128, 95.31.0.253/32
* __eth10__: 2a00:31::6/126, 95.31.0.6/30
* __eth20__: 2a00:31::a/126, 95.31.0.10/30
* __eth30__: 2a00:31:b::1/126, 95.31.200.1/30

__as32_r1:__

* __lo__: 2a00:32::254/128, 95.32.0.254/32
* __eth10__: 2a00:32::1/126, 95.32.0.1/30
* __eth20__: 2a00:32:a::1/64, 95.32.100.1/24

__as33_r1:__

* __lo__: 2a00:33::254/128, 95.33.0.254/32
* __eth10__: 2a00:33::1/126, 95.33.0.1/30
* __eth20__: 2a00:33:a::1/64, 95.33.100.1/24


## Testing

Connectivity and routes may be tested using __docker exec__ utility directly in routers container or if you prefer you may have a host like experience using host containers. Both methods are described below.

### Using routers

You may test if what you have configured is correct running tests such as ping or traceroute in routers shell:

```bash
$ docker exec <router container name> ping -I <src address> <dst address>
$ docker exec <router container name> traceroute <dst address>

## example:
$ docker exec asXX_r1 ping -I 2a00:11::1 2a00:11:2::1
$ docker exec asXX_r1 traceroute 2a00:11:2::1
```

### Using "hosts"

This project contains a docker-compose file with a host definition making even easier to run a host container. The file called _host.yml_ is on the root directory of this project.

For each host you need a new terminal session. The command below starts a host container without any network interface:

```bash
$ cd <project_root>
$ docker-compose -f host.yml run --rm --name <host name> host

## examples:
$ docker-compose -f host.yml run --rm --name h1 host
$ docker-compose -f host.yml run --rm --name h2 host
```

After host containers are started you need to connect them to switches using __ovs-docker__ tool:

```bash
root# ovs-docker add-port <virtual switch> eth0 <host name>

## examples:
root# ovs-docker add-port asXX_sw1 eth0 h1
root# ovs-docker add-port asXX_sw1 eth0 h2
```

In the example above _h1_ and _h2_ are connected to the same virtual switch where a router also is. Hosts have L2 connectivity but not L3. Hosts need to be configured manually:

```bash
## example:
host-shell# ip addr add 2a00:11:1::10/64 dev eth0
host-shell# ip addr add 95.11.2.10/24 dev eth0
host-shell# ip route add default via 2a00:11:1::1 dev eth0
host-shell# ip route add default via 95.11.2.1 dev eth0
```

When you exit from the host typing __exit__ it will be destroyed because we are using the __--rm__ argument.
