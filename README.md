
This project enables fastly create and destroy network routing laboratories based on Docker and Bash Scripts. Routers are emulated using [Quagga](http://www.nongnu.org/quagga/).

In order to run any Lab will need the following packages installed in your system:

* [Docker](https://www.docker.com/)
* [Docker-compose](https://docs.docker.com/compose/)
* [Open vSwitch](http://openvswitch.org/)
* [OVS-Docker](https://github.com/openvswitch/ovs/blob/master/utilities/ovs-docker)

Having a good terminal emulator such as [Terminator](http://gnometerminator.blogspot.com.br/p/introduction.html) or [Tmux](https://tmux.github.io/) would be useful because you will need a different terminal session for each router you want to connect in.


# Running labs

Each directory is a different laboratory; the directory contains a README file describing the laboratory with a diagram and addressing information of each router. It also contains a bash script to start, destroy and reset the lab.

## Starting a lab

Get into the directory of the desired lab and run (__as root__):

```bash
root# bash run up
```

The command above creates routers containers, routers data volumes, OVS switches and connects them all. Every change you do in routers  configurations (using _write mem_ command) are saved in the corresponding data volume. Even when you stop the lab saved configuration will remain; it will be lost only when you reset the lab.


## Stopping a lab

```bash
root# bash run down
```

The command above destroys containers and switches, but does not remove data volumes; sabed configuration will persist for the next execution.


## Reseting

Reseting means to go back to the original state. It will erase every saved configuration.

```bash
root# bash run reset
```

## [Re]Building

If you had ran any lab and then you changed any router's configuration editing tha base files at lab's config directory you must rebuild its image in order to get changes applied. First stop and reset the lab, so remove lab's images; when you run the lab again Docker will build the new image.

See following commands:

```bash
root# bash run down
root# bash run reset
#
# Check existing images
root# docker images
# Delete lab's images
root# docker rmi <image ID>
#
# You may delete all the images at once running
root# docker rmi $(docker images -q)
```

# Connecting to routers

In the laboratory's README file you can see the address to connect to the router; each router has an address in a Docker network to be used to connect to it.

For each router you need a terminal session. Running a lab will be like seem in the figure below:

![Terminal example](/images/terminal_split.png)

Use the IP address from the README file and the daemon port to connect:

```bash
user$ telnet <ip address> port
```

Daemons' ports are:

* 2601: Zebra daemon - Addressing and static routes configuration
* 2604: OSPFv4 daemon
* 2605: BGP daemon
* 2606: OSPFv6 daemon


# Using host containers in labs

When you run a lab host containers are not included. Althought you may run host containers (containers running bash) and connect them to the switch/network you want. Each lab's README files contain instructions showing how to run, connect and configure host containers.


# SDN - Software-Defined Networking

Open vSwitch works as an SDN switch. You may set flows in virtual switches using [OpenFlow](https://www.opennetworking.org/sdn-resources/openflow) or [OVSDB](https://tools.ietf.org/html/rfc7047).

Each laboratory creates a set of virtual switches, some of them are shown in the laboratories' diagrams. You may see all created switches running __ovs-vsctl show__.

If you want to connect a virtual switch to an OpenFlow controller run the following:

```bash
root# ovs-vsctl set-controller <bridge/switch name> tcp:<controller IP>:6653

## example:
root# ovs-vsctl set-controller lab1_sw1 tcp:127.0.0.1:6653
```

If you want the virtual switch to work only based on OpenFlow you must configure the switch as follow:

```bash
root# ovs-vsctl set-fail-mode <bridge/switch name> secure

## example:
root# ovs-vsctl set-fail-mode lab1_sw1 secure
```

The command above disables the L2 normal forwarding so if the switch loses connection to the controller it will not forward anything when the last flow-entry expires.
